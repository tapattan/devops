#!/usr/bin/env/python

def sayHello():
    return "Hello world!"

def add (a,b):
    return a + b

def power (a,b):
    m = 1 
    for i in range(b):
      m = m*a 
    return m


def factorial (n):
    m = 1 
    for i in range(1,n+1):
      m = m*i
    return m