import unittest
import myfun

# python3 -m unittest -v
class TestCallMethods(unittest.TestCase):

    def test_add(self):
        theSum = myfun.add(2,3)
        self.assertEqual(theSum, 5, "Sum should be five ...")

    def test_power(self):
        thePower = myfun.power(2,10)
        self.assertEqual(thePower, 1024, "2^10 should be 1024 ...")

    def test_sayHello(self):
        greetings = myfun.sayHello()
        self.assertEqual(greetings, "Hello world!" , "Greetings should be: Hello world!")

    def test_factorial(self):
        theFactorial = myfun.factorial(5)
        self.assertEqual(theFactorial, 120, "5! should be 120")

        theFactorial = myfun.factorial(10)
        self.assertEqual(theFactorial, 3628800, "10! should be 3628800")

if __name__ == '__main__':
    unittest.main()